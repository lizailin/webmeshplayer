const stringToHalf = function (string) {
    let n = parseInt(string)
    n = n & 0xffff;

    let sign = n >> 15
    sign = sign ? -1 : 1
    let exponent = (n >> 10) & 0x1f
    let fraction = n & 0x3ff

    let fr = 1
    if (exponent == 0x1f) {
        return fraction == 0 ? sign * Infinity : NaN;
    } else if (exponent == 0) {
        fr = 0;
        exponent = -14
    } else {
        exponent = exponent - 0xf
    }

    for (let i = 0; i < 10; i++) {
        fr += ((fraction >> (10 - i - 1)) & 0x1) * Math.pow(1 / 2, i + 1)
    }

    let r = sign * fr * Math.pow(2, exponent)
    // 小数点后 6 位精度，四舍五入
    // let a = r.toFixed(6)
    // ler r = parseFloat(a)
    return r
}


const arrayEqual = function (arr1, arr2) {
    if (arr1.length != arr2.length) {
        return false
    }
    for (var i = 0; i < arr1.length; ++i) {
        if (arr1[i] !== arr2[i]) {
            return false
        }
    }
    return true
}


// ############## decode sei ################
const Type = {
    INT32: 0,
    FLOAT32: 1,
    USHORT16: 2,
    HALF16: 3,
}

const dict = {
    // 其中 65 - 70， 97 - 102 (a - f) 不区分大小写
    48: '0',
    49: '1',
    50: '2',
    51: '3',
    52: '4',
    53: '5',
    54: '6',
    55: '7',
    56: '8',
    57: '9',

    65: 'A', // A
    66: 'B', // B
    67: 'C', // C
    68: 'D', // D
    69: 'E', // E
    70: 'F', // F

    97: 'a', // a
    98: 'b', // b
    99: 'c', // c
    100: 'd', // d
    101: 'e', // e
    102: 'f', // f
    103: 'g', // g
    104: 'h', // h
    105: 'i', // i
    106: 'j', // j
    107: 'k', // k
    108: 'l', // l
    109: 'm', // m
    110: 'n', // n
    111: 'o', // o
    112: 'p', // p
    113: 'q', // q
    114: 'r', // r
    115: 's', // s
    116: 't', // t
    117: 'u', // u
    118: 'v', // v
    119: 'w', // w
    120: 'x', // x
    121: 'y', // y
    122: 'z', // z
}


const lenFromArray = function (array) {
    let a = array

    let s1 = '0x' + a[0] + a[1]
    let s2 = '0x' + a[2] + a[3]
    let s3 = '0x' + a[4] + a[5]
    let s4 = '0x' + a[6] + a[7]

    let n1 = parseInt(s1)
    let n2 = parseInt(s2)
    let n3 = parseInt(s3)
    let n4 = parseInt(s4)
    let r = (n4 * 256 * 256 * 256) + (n3 * 256 * 256) + (n2 * 256) + n1
    return r
}

const hex2str = function (array) {
    let l = array
    let r = []
    for (let i = 0; i < l.length; i++) {
        // 从 buffer 里取出元素，hex 就变成 int
        let k = l[i]
        // 将 int 转成对应的 str
        let n = dict[k]
        r.push(n)
    }
    return r
}

const parseSei = function (len_data) {
    let l = len_data
    let a = hex2str(l)
    let r = lenFromArray(a)
    return r
}

const parseTag = function (tag_buffer) {
    let t = tag_buffer
    let a = hex2str(t)
    let r = a.join("")
    return r
}

const parseHeader_V_F_TC_FTC = function (buffer) {
    // 每个数据段，由 5 部分组成
    // len：数据总长，长度  8
    // h：二维数组高度，长度 8
    // w：二维数组宽度，长度 8
    // type: 该数据段类型，现有 4 种，如下
    // INT32
    // FLOAT32
    // USHORT16
    // HALF16
    let v = buffer
    let l1 = hex2str(v.slice(0, 8))
    let len = lenFromArray(l1)

    let l2 = hex2str(v.slice(8, 16))
    let h = lenFromArray(l2)

    let l3 = hex2str(v.slice(16, 24))
    let w = lenFromArray(l3)

    let l4 = hex2str(v.slice(24, 32))
    let type = lenFromArray(l4)

    let r = []
    if (type === Type.INT32 || type === Type.FLOAT32) {
        return
    } else if (type === Type.USHORT16) {
        let size = h * w * 4
        let u = hex2str(v.slice(32, size + 32))
        for (let i = 0; i < u.length; i += 4) {
            let s1 = '0x' + u[i] + u[i + 1]
            let s2 = '0x' + u[i + 2] + u[i + 3]
            let s = parseInt(s1) + s2 * 256
            r.push(s)
        }
    } else if (type === Type.HALF16) {
        let size = h * w * 4
        let u = hex2str(v.slice(32, size + 32))

        for (let i = 0; i < u.length; i += 4) {
            let c = '0x' + u[i + 2] + u[i + 3] + u[i] + u[i + 1]
            let s = stringToHalf(c)
            r.push(s)
        }
    }
    return [len, r]
}

const parseV_F_TC_FTC = function (buffer) {
    // 每个 v_t_f 中都有 4 部分, 分别对应 obj 文件的 v，vn，vt，f
    let b = buffer
    let index = 0

    let [len_v, v] = parseHeader_V_F_TC_FTC(b)
    let d_v = formatData_v(v)
    index += len_v

    let [len_fv, fv] = parseHeader_V_F_TC_FTC(b.slice(index))
    index += len_fv

    let [len_vt, vt] = parseHeader_V_F_TC_FTC(b.slice(index))
    let d_vt = formatData_vt(vt)
    index += len_vt

    let [len_fvt, fvt] = parseHeader_V_F_TC_FTC(b.slice(index))

    let fvn = []
    let d_f = formatData_f(fv, fvt, fvn)
    let r = d_v.concat(d_vt, d_f)
    return r
}

const formatData_v = function (buffer) {
    let b = buffer
    let len = b.length / 3

    let v1 = b.slice(0, len)
    let v2 = b.slice(len, len * 2)
    let v3 = b.slice(len * 2, len * 3)
    let r = []
    for (let i = 0; i < len; i++) {
        r.push(['v', v1[i], v2[i], v3[i]])
    }
    return r
}

const formatData_vt = function (buffer) {
    let b = buffer
    let len = b.length / 2

    let v1 = b.slice(0, len)
    let v2 = b.slice(len)
    let r = []
    for (let i = 0; i < len; i++) {
        r.push(['vt', v1[i], v2[i]])
    }
    return r
}

const formatData_f = function (fv, fvt, fvn) {
    let [vl, vtl, vnl] = [fv.length / 3, fvt.length / 3, fvn.length / 3]
    if (vnl === 0) {
        fvn = Array(fv.length).fill('')
        vnl = vl
    }
    let len = vl

    let v1 = fv.slice(0, vl)
    let v2 = fv.slice(vl, vl * 2)
    let v3 = fv.slice(vl * 2)

    let vt1 = fvt.slice(0, vtl)
    let vt2 = fvt.slice(vtl, vtl * 2)
    let vt3 = fvt.slice(vtl * 2)

    let vn1 = fvn.slice(0, vnl)
    let vn2 = fvn.slice(vnl, vnl * 2)
    let vn3 = fvn.slice(vnl * 2)
    let r = []
    for (let i = 0; i < len; i++) {
        let a1 = [(v1[i] + 1).toString(), (vt1[i] + 1).toString(), vn1[i]]
        let a2 = [(v2[i] + 1).toString(), (vt2[i] + 1).toString(), vn2[i]]
        let a3 = [(v3[i] + 1).toString(), (vt3[i] + 1).toString(), vn3[i]]
        // let s = [a1, a2, a3]
        let s = ['f', a1, a2, a3]
        r.push(s)
    }
    return r
}

const lenFromData = function (len_data) {
    let l = len_data
    let a = hex2str(l)
    let r = lenFromArray(a)
    return r
}

const parseData = function (buffer) {

    // obj data 由 3 部分组成，
    // 数据段长度：长度 8。 
    // tag：长度 8，可解析为 "ushl" 或 "inhl"
    // v_t_f 数据
    let d = buffer
    let len = lenFromData(d.slice(0, 8))
    let tag = d.slice(8, 16)
    parseTag(tag)

    // 解析 v_t_f
    let v_data = d.slice(16)
    let r = parseV_F_TC_FTC(v_data)
    return r
}

const seiFromMp4 = function (data) {
    let d = data
    let uid = [0xca, 0x38, 0xb7, 0xe7]
    // log('uid', uid, d.length)
    let r = []
    // 查找所有的标志 uid
    for (let i = 0; i < d.length - uid.length; i++) {
        let a = [d[i], d[i + 1], d[i + 2], d[i + 3]]
        let t = arrayEqual(a, uid)
        // 找到
        if (t) {
            // 解析sei，
            // sei 有 3 部分组成，uid ：长度 16； obj length：长度  8； obj data： 长度  由 obj length 计算得来
            // obj length
            let start = i + 16
            let end = start + 8
            let la = d.slice(start, end)
            let len = parseSei(la)

            // obj data
            let obj_data = d.slice(end, end + len)
            let s = parseData(obj_data)
            r.push(s)
        }
    }
    return r
}

//  ########## decode mp4 ##########
const dataFromMp4 = function (data) {
    let testData = data
    const ds = ffmpeg({
        MEMFS: [{ name: "mesh.mp4", data: testData }],
        // ffmpeg.js 只支持 jpg
        arguments: ["-i", "mesh.mp4", "out%d.jpg", "-hide_banner"],
        stdin: function () {
        },
    })
    return ds.MEMFS
}

const parseMp4 = function (data) {
    let obj = seiFromMp4(data)
    return obj
}

export { parseMp4 }